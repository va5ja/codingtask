# Coding Task - REST API

# Requirements
- The project was created with the latest version of Symfony framework (currently 4.2)
- The sample data (catalog.sqlite) was assumed to be dynamic
  - I assumed that the column names could change
  - I assumed that the amount of columns could vary
  - I assumed that there's one index set and this index is the primary key
 
# Specifications
- The API supports basic data fetching from the provided SQLite file in a paginated fashion (10 items per page using `?p={page_number}`)
- The API tries to follow the RESTful principles
- The API accepts the basic hardcoded token as security (defined inside `services.yaml` => api_security_token: `66LJ$3ItJ8K#HLbG`)
- Endpoints that do not exist result in a 404 response
- Data is returned in JSON format with appropriate response codes and headers
- A user can specify search parameters in a form such as `q[description][]=sodium&q[description][]=alcohol&q[title]=hello`
- A user can specify as many search terms as they like
- A user can:
  - request a full data set: `/api/v1/dataset?q[title]=alcohol&p=1` over `GET`
  - request a single item:  `/api/v1/dataset/{id}` over `GET`
  - add a single item: `/api/v1/dataset` over `POST`
  - delete a single item: `/api/v1/dataset/{id}` over `DELETE`
  - update a single item: `/api/v1/dataset/{id}` over `PUT` or `PATCH`
  - fetch options over `OPTIONS`
- All actions are also executable using console command `app:api`
  - help: `php bin/console app:api --help`
  - create: `php bin/console app:api -a create -d '{"column": "value"}'` 
  - read: `php bin/console app:api -a read -s 'q[description][]=sodium&q[description][]=alcohol&q[title]=hello' -p 1` 
  - update: `php bin/console app:api -a update -i 3 -d '{"column": "value"}'` 
  - delete: `php bin/console app:api -a delete -i 3`

# External dependencies
- I planned to use the `doctrine/orm` library but because of the dynamic database assumption above I decided against it and only used the `doctrine/dbal` part instead
- I used a popular `whiteoctober/Pagerfanta` library for pagination because I didn't want to reinvent the wheel
- I could have used the `FriendsOfSymfony/FOSRestBundle` bundle which provides some helful tools but I rather tried to implement all the logic myself

# Notes
- The SQLite database file (catalog.sqlite) needs to be placed inside `/var` directory
- CORS can be applied on the server (over Apache/nginx etc. directives) or using `errorResponse()` and `successResponse()` methods inside `ApiController`
- The project also includes `docker-compose.yml` for running the app on port `8001` (note: the database file needs to have write permissions)
- The app can also be ran locally using Symfony's internal server with `php bin/console server:run`
  
# Improvements
- The unit tests can be improved, the `DataService` dealing with the database can be mocked etc.
- The console command's output can be improved with clearer response messages
- The app can be made more robust with enhanced exception handling and improved validation