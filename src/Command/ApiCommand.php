<?php

namespace App\Command;

use App\Controller\ApiControllerTrait;
use App\Service\DataService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Command offering the same functionality than the API.
 * Actions:
 *  - create
 *  - read
 *  - update
 *  - delete
 *
 * Class ApiCommand
 * @package App\Command
 */
class ApiCommand extends Command
{
    use ApiControllerTrait;

    protected static $defaultName = 'app:api';

    const ALL_COLUMNS_REQUIRED_METHODS = ['create'];

    /** @var DataService */
    protected $dataService;

    /** @var ValidatorInterface */
    protected $validator;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var RouterInterface */
    protected $router;

    /**
     * ApiCommand constructor.
     * @param DataService $dataService
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     * @param RouterInterface $router
     */
    public function __construct(
        DataService $dataService,
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        RouterInterface $router
    ) {
        $this->dataService = $dataService;
        $this->validator = $validator;
        $this->serializer = $serializer;
        $this->router = $router;

        parent::__construct();
    }

    /**
     * Input options configuration.
     */
    protected function configure()
    {
        $this
            ->setDescription('Command line API manipulator.')
            ->setHelp('This command allows you to manipulate the API over command line.')
            ->addOption('action', 'a', InputOption::VALUE_REQUIRED, 'Action (create, read, update, delete)', 'read')
            ->addOption('id', 'i', InputOption::VALUE_REQUIRED, 'Specific ID')
            ->addOption('data', 'd', InputOption::VALUE_REQUIRED, 'Data in JSON format')
            ->addOption('search', 's', InputOption::VALUE_REQUIRED, 'Search query terms (q[description]=sodium)')
            ->addOption('page', 'p', InputOption::VALUE_REQUIRED, 'Current page', 1);
    }

    /**
     * Main execution block of the action.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $action = $input->getOption('action');
        $id = $input->getOption('id') ? $input->getOption('id') : null;
        $data = $input->getOption('data') ? $input->getOption('data') : '{}';
        $query = $input->getOption('search') ? $input->getOption('search') : '';
        $page = $input->getOption('page');

        parse_str($query, $queryArray);

        switch ($action) {
            case 'create':
                $response = $this->createDataset($data, $action);
                break;
            case 'read':
                $response = $this->readDataset($id, $action, !empty($queryArray['q']) ? $queryArray['q'] : [], $page);
                break;
            case 'update':
                $response = $this->updateDataset($id, $data, $action);
                break;
            case 'delete':
                $response = $this->deleteDataset($id);
                break;
            default:
                $response = ['error' => ['code' => 1, 'messages' => ['Invalid action.']]];
        }

        $table = new Table($output);
        $rows = [];
        $isErrorResponse = false;

        if (isset($response['error'])) {
            $isErrorResponse = true;
            $headers = ['Errors'];

            foreach ($response['error']['messages'] as $row) {
                $rows[] = [$row];
            }
        } else {
            $headers = !empty($response['results'][0]) ? array_keys($response['results'][0]) : [];

            foreach ($response['results'] as $row) {
                $rows[] = array_map(
                    function ($value) {
                        return mb_strimwidth($value, 0, 32, '…');
                    },
                    array_values($row)
                );
            }
        }

        $table->setHeaders($headers)->setRows($rows);

        $table->render();

        if ($isErrorResponse) {
            exit($response['error']['code']);
        }
    }

    /**
     * Response data decorator in case of errors.
     *
     * @param array $data
     * @param int $status
     * @return array
     */
    protected function errorResponse(array $data, int $status): array
    {
        return ['error' => ['code' => $status - 365, 'messages' => $data]];
    }

    /**
     * Response data decorator in case of success.
     *
     * @param array $data
     * @return array
     */
    protected function successResponse(array $data): array
    {
        return $data;
    }

    /**
     * Generates a URL from the given parameters.
     *
     * @param string $route
     * @param array $parameters
     * @return string
     */
    protected function generateUrl(string $route, array $parameters = []): string
    {
        return $this->router->generate($route, $parameters);
    }
}
