<?php

namespace App\Controller;

use App\Service\DataService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ApiController
 * @package App\Controller
 */
class ApiController extends AbstractController
{
    use ApiControllerTrait;

    const ALL_COLUMNS_REQUIRED_METHODS = ['POST', 'PUT'];

    /** @var DataService */
    protected $dataService;

    /** @var ValidatorInterface */
    protected $validator;

    /** @var SerializerInterface */
    protected $serializer;

    /**
     * ApiController constructor.
     * @param DataService $dataService
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     */
    public function __construct(
        DataService $dataService,
        ValidatorInterface $validator,
        SerializerInterface $serializer
    ) {
        $this->dataService = $dataService;
        $this->validator = $validator;
        $this->serializer = $serializer;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function createAction(Request $request): JsonResponse
    {
        return $this->createDataset($request->getContent(), $request->getMethod());
    }

    /**
     * @param Request $request
     * @param mixed $id
     * @return JsonResponse
     */
    public function readAction(Request $request, $id): JsonResponse
    {
        $searchQueries = $request->query->has('q') ? $request->query->get('q') : [];
        $page = $request->query->has('p') ? $request->query->get('p') : 1;

        return $this->readDataset($id, $request->getMethod(), $searchQueries, $page);
    }

    /**
     * @param Request $request
     * @param mixed $id
     * @return JsonResponse
     */
    public function updateAction(Request $request, $id): JsonResponse
    {
        return $this->updateDataset($id, $request->getContent(), $request->getMethod());
    }

    /**
     * @param mixed $id
     * @return JsonResponse
     */
    public function deleteAction($id): JsonResponse
    {
        return $this->deleteDataset($id);
    }

    /**
     * Response data decorator in case of errors.
     *
     * @param array $data
     * @param int $status
     * @param array $headers
     * @return JsonResponse
     */
    protected function errorResponse(array $data, int $status, array $headers = []): JsonResponse
    {
        return new JsonResponse(['error' => ['code' => $status, 'messages' => $data]], $status, $headers);
    }

    /**
     * Response data decorator in case of success.
     *
     * @param array $data
     * @param int $status
     * @param array $headers
     * @return JsonResponse
     */
    protected function successResponse(array $data, int $status, array $headers = []): JsonResponse
    {
        return new JsonResponse($data, $status, $headers);
    }
}
