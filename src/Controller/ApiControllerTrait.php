<?php

namespace App\Controller;

use App\Model\Dataset;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Trait with methods used both in the APIController and the ApiCommand.
 *
 * Trait ApiControllerTrait
 * @package App\Controller
 */
trait ApiControllerTrait
{
    /**
     * @param string $data
     * @param string $method
     * @return mixed
     */
    public function createDataset(string $data, string $method)
    {
        $requestDataset = $this->deserialize($data);

        $validationErrors = $this->validate($requestDataset, $method);
        if ($validationErrors) {
            return $this->errorResponse($validationErrors, 400);
        }

        $createdDataset = $this->dataService->create($requestDataset->getData());

        $primaryKeyName = $this->dataService->getPrimaryKeyName();
        $resourceLocation = $this->generateUrl(
            'api_dataset_read',
            [$primaryKeyName => $createdDataset['results'][0][$primaryKeyName]]
        );

        return $this->successResponse($createdDataset, 201, ['Location' => $resourceLocation]);
    }

    /**
     * @param mixed $id
     * @param string $method
     * @param array $searchQueries
     * @param int $page
     * @return mixed
     */
    public function readDataset($id, string $method, array $searchQueries, int $page)
    {
        if ($searchQueries) {
            $requestDataset = $this->denormalize($searchQueries);

            $validationErrors = $this->validate($requestDataset, $method, true);
            if ($validationErrors) {
                return $this->errorResponse($validationErrors, 400);
            }

            $searchQueries = $requestDataset->getData();
        }

        $readDataset = $this->dataService->read($id, $searchQueries, $page);

        if ($id && !$readDataset['results']) {
            return $this->errorResponse(['Resource not found.'], 404);
        }

        return $this->successResponse($readDataset, 200);
    }

    /**
     * @param mixed $id
     * @param string $data
     * @param string $method
     * @return mixed
     */
    public function updateDataset($id, string $data, string $method)
    {
        $requestDataset = $this->deserialize($data);

        $validationErrors = $this->validate($requestDataset, $method);
        if ($validationErrors) {
            return $this->errorResponse($validationErrors, 400);
        }

        $updatedDataset = $this->dataService->update($id, $requestDataset->getData());

        if (!$updatedDataset['updated']) {
            return $this->errorResponse(['Resource not found.'], 404);
        }

        return $this->successResponse($updatedDataset, 200);
    }

    /**
     * @param mixed $id
     * @return mixed
     */
    public function deleteDataset($id)
    {
        $deletedDataset = $this->dataService->delete($id);

        if (!$deletedDataset['deleted']) {
            return $this->errorResponse(['Resource not found.'], 404);
        }

        return $this->successResponse($deletedDataset, 200);
    }

    /**
     * Deserialize the request's JSON data into a Dataset model.
     *
     * @param string $data
     * @return Dataset
     */
    protected function deserialize(string $data): Dataset
    {
        /** @var Dataset $dataset */
        $dataset = $this->serializer->deserialize($data, Dataset::class, 'json');
        $dataset->setWhitelistColumns($this->dataService->getColumns(), $this->dataService->getPrimaryKeyName());

        return $dataset;
    }

    /**
     * Denormalize the request's array search query into a Dataset model.
     *
     * @param array $data
     * @return Dataset
     */
    protected function denormalize(array $data): Dataset
    {
        /** @var Dataset $dataset */
        $dataset = $this->serializer->denormalize($data, Dataset::class);
        $dataset->setWhitelistColumns($this->dataService->getColumns(), $this->dataService->getPrimaryKeyName());

        return $dataset;
    }

    /**
     * Validates the request by inspecting the possible missing or unknown properties
     * and/or possible property type violations.
     *
     * @param Dataset $dataset
     * @param string $method
     * @param bool $requestOnly
     * @return array
     */
    protected function validate(Dataset $dataset, string $method, $requestOnly = false): array
    {
        $errors = [];

        $requestErrors = $dataset->validateRequest(
            in_array($method, self::ALL_COLUMNS_REQUIRED_METHODS) ? true : false
        );
        foreach ($requestErrors as $requestError) {
            $errors[] = $requestError;
        }

        if (!$requestOnly) {
            /** @var ConstraintViolationInterface[] $validationErrors */
            $validationErrors = $this->validator->validate($dataset);
            foreach ($validationErrors as $validationError) {
                $errors[] = $validationError->getMessage();
            }
        }

        return $errors;
    }
}