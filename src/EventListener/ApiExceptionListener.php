<?php

namespace App\EventListener;

use App\Exception\ApiExceptionInterface;
use App\Exception\UserInputExceptionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ApiExceptionListener
 * @package App\EventListener
 */
class ApiExceptionListener
{
    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof NotFoundHttpException) {
            $event->setResponse(
                new JsonResponse(
                    ['error' => ['code' => $exception->getStatusCode(), 'messages' => [$exception->getMessage()]]],
                    $exception->getStatusCode()
                )
            );

            return;
        }

        if (!$exception instanceof ApiExceptionInterface) {
            return;
        }

        $code = $exception instanceof UserInputExceptionInterface ? 400 : 500;

        $event->setResponse(
            new JsonResponse(['error' => ['code' => $code, 'messages' => [$exception->getMessage()]]], $code)
        );
    }
}