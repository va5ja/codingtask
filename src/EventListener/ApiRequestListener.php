<?php

namespace App\EventListener;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class ApiRequestListener
 * @package App\EventListener
 */
class ApiRequestListener
{
    /** @var RouterInterface */
    protected $router;

    /** @var ParameterBagInterface */
    protected $params;

    /**
     * ApiRequestListener constructor.
     * @param RouterInterface $router
     * @param ParameterBagInterface $params
     */
    public function __construct(RouterInterface $router, ParameterBagInterface $params)
    {
        $this->router = $router;
        $this->params = $params;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();
        $headers = $request->headers;

        /**
         * OPTIONS request handling
         */
        if ($request->isMethod('OPTIONS')) {
            $routeCollection = $this->router->getRouteCollection();
            $requestPath = $routeCollection->get($request->get('_route'))->getPath();
            $methods = [];

            foreach ($routeCollection as $route) {
                if ($route->getPath() === $requestPath) {
                    $methods = array_merge($methods, $route->getMethods());
                }
            }

            $event->setResponse(
                new JsonResponse(
                    null, 200, [
                        'Allow' => implode(',', array_unique($methods)),
                        'Access-Control-Allow-Origin' => '*',
                    ]
                )
            );
        } else {
            /**
             * Security token handling
             */
            if (!$headers->has('x-security-token')) {
                $event->setResponse(
                    new JsonResponse(['error' => ['code' => 400, 'messages' => ['Security token is required.']]], 400)
                );
            } elseif ($headers->get('x-security-token') !== $this->params->get('api_security_token')) {
                $event->setResponse(
                    new JsonResponse(['error' => ['code' => 400, 'messages' => ['Invalid security token.']]], 400)
                );
            }
        }
    }
}