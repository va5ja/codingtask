<?php

namespace App\Exception;

/**
 * Interface ApiExceptionInterface
 * @package App\Exception
 */
interface ApiExceptionInterface
{
    /**
     * @return mixed
     */
    public function getMessage();
}