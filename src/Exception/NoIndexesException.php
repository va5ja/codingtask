<?php

namespace App\Exception;

/**
 * Exception in case the database table has no indexes
 *
 * Class NoIndexesException
 * @package App\Exception
 */
class NoIndexesException extends \Exception implements ApiExceptionInterface
{
}