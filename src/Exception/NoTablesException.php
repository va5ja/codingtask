<?php

namespace App\Exception;

/**
 * Exception in case the database has no tables
 *
 * Class NoTablesException
 * @package App\Exception
 */
class NoTablesException extends \Exception implements ApiExceptionInterface
{
}