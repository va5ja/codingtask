<?php

namespace App\Exception;

/**
 * Interface UserInputExceptionInterface
 * @package App\Exception
 */
interface UserInputExceptionInterface
{
    /**
     * @return mixed
     */
    public function getMessage();
}