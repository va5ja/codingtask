<?php

namespace App\Model;

use Doctrine\DBAL\Schema\Column;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * Model holding the request data and the whitelisted
 * columns retrieved from the database table specs.
 *
 * Class Dataset
 * @package App\Model
 */
class Dataset
{
    /** @var array */
    protected $requestColumns = [];

    /** @var Column[] */
    protected $whitelistColumns = [];

    /** @var array */
    protected $columnTypeToAssertTypeMapping = [
        'array' => 'array',
        'bigint' => 'integer',
        'boolean' => 'bool',
        'decimal' => 'double',
        'integer' => 'integer',
        'object' => 'object',
        'smallint' => 'integer',
        'string' => 'string',
        'text' => 'string',
    ];

    /**
     * @param string $property
     * @param mixed $value
     */
    public function __set(string $property, $value): void
    {
        $this->requestColumns[$property] = $value;
    }

    /**
     * @param string $property
     * @return mixed
     */
    public function __get(string $property)
    {
        return $this->requestColumns[$property];
    }

    /**
     * Gets data from the request.
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->requestColumns;
    }

    /**
     * @param Column[] $columns
     * @param string $primaryKeyName
     */
    public function setWhitelistColumns(array $columns, string $primaryKeyName): void
    {
        foreach ($columns as $column) {
            $this->whitelistColumns[$column->getName()] = $column;
        }

        unset($this->whitelistColumns[$primaryKeyName]);
    }

    /**
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata): void
    {
        $metadata->addConstraint(new Assert\Callback('validateColumns'));
    }

    /**
     * Validates the request by inspecting the possible missing or unknown properties.
     *
     * @param bool $allColumnsRequired
     * @return array
     */
    public function validateRequest(bool $allColumnsRequired = false): array
    {
        $errors = [];

        if ($allColumnsRequired) {
            $missingColumns = array_diff_key($this->whitelistColumns, $this->requestColumns);
            if ($missingColumns) {
                $errors[] = 'Missing properties ('.implode(', ', array_keys($missingColumns)).').';
            }
        }

        $invalidColumns = array_diff_key($this->requestColumns, $this->whitelistColumns);
        if ($invalidColumns) {
            $errors[] = 'Invalid properties ('.implode(', ', array_keys($invalidColumns)).').';
        }

        return $errors;
    }

    /**
     * Validates the request by inspecting the possible property type violations.
     *
     * @param ExecutionContextInterface $context
     */
    public function validateColumns(ExecutionContextInterface $context): void
    {
        $constraints = [];

        foreach (array_keys($this->requestColumns) as $column) {
            if (isset($this->whitelistColumns[$column])) {
                $constraints[$column] = [
                    new Assert\Type(
                        [
                            'type' => $this->getAssertTypeByColumnName($column),
                            'message' => 'The value {{ value }} of property "'.$column.'" is not a valid {{ type }}.',
                        ]
                    ),
                ];
            }
        }

        $violations = $context->getValidator()->validate(
            $this->requestColumns,
            new Assert\Collection(
                [
                    'fields' => $constraints,
                    'allowExtraFields' => true,
                    'allowMissingFields' => true,
                ]
            )
        );

        foreach ($violations as $violation) {
            $context->getViolations()->add($violation);
        }
    }

    /**
     * Gets assertion type judging from the column type (defaults to string).
     *
     * @param string $columnName
     * @return string
     */
    protected function getAssertTypeByColumnName(string $columnName): string
    {
        $columnType = $this->whitelistColumns[$columnName]->getType()->getName();
        $assertType = 'string';

        if (isset($this->columnTypeToAssertTypeMapping[$columnType])) {
            $assertType = $this->columnTypeToAssertTypeMapping[$columnType];
        }

        return $assertType;
    }
}