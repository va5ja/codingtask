<?php

namespace App\Service;

use App\Exception\NoIndexesException;
use App\Exception\NoTablesException;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Schema\Column;
use Pagerfanta\Adapter\DoctrineDbalSingleTableAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Service providing the database read and write operations.
 *
 * Class DataService
 * @package App\Service
 */
class DataService
{
    /** @var Connection */
    protected $connection;

    /** @var ParameterBagInterface */
    protected $params;

    /** @var Table */
    protected $table;

    /**
     * DataService constructor.
     * @param Connection $connection
     * @param ParameterBagInterface $params
     * @throws NoTablesException
     */
    public function __construct(Connection $connection, ParameterBagInterface $params)
    {
        $this->connection = $connection;
        $this->params = $params;
        $this->setTable();
    }

    /**
     * @param array $data
     * @return array
     * @throws NoIndexesException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function create(array $data): array
    {
        $highestPrimaryKey = $this->connection->createQueryBuilder()
            ->select('MAX(CAST(d.'.$this->getPrimaryKeyName().' AS INTEGER)) AS max')
            ->from($this->table->getName(), 'd');

        $data[$this->getPrimaryKeyName()] = $highestPrimaryKey->execute()->fetchColumn(0) + 1;;

        $create = $this->connection->insert($this->table->getName(), $data);

        return ['results' => [$data], 'created' => (bool)$create];
    }

    /**
     * @param string|null $id
     * @param array $searchQueries
     * @param int $page
     * @return array
     * @throws NoIndexesException
     */
    public function read($id, array $searchQueries, int $page): array
    {
        if ($id) {
            $queryBuilder = $this->connection->createQueryBuilder()
                ->select('*')
                ->from($this->table->getName(), 'd')
                ->where('d.'.$this->getPrimaryKeyName().' = ?')
                ->setParameter(0, $id);
        } else {
            $queryBuilder = $this->connection->createQueryBuilder()
                ->select('*')
                ->from($this->table->getName(), 'd');
        }

        if ($searchQueries) {
            foreach ($searchQueries as $column => $terms) {
                foreach ((array)$terms as $term) {
                    $queryBuilder->andWhere('d.'.$column." LIKE '%$term%'");
                }
            }
        }

        $pagination = new Pagerfanta(
            new DoctrineDbalSingleTableAdapter($queryBuilder, 'd.'.$this->getPrimaryKeyName())
        );
        $pagination->setMaxPerPage($this->params->get('api_max_per_page'));
        $pagination->setCurrentPage($page);

        return ['results' => $pagination->getCurrentPageResults(), 'has_next_page' => $pagination->hasNextPage()];
    }

    /**
     * @param string $id
     * @param array $data
     * @return array
     * @throws NoIndexesException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function update(string $id, array $data): array
    {
        $update = $this->connection->update($this->table->getName(), $data, [$this->getPrimaryKeyName() => $id]);
        $data[$this->getPrimaryKeyName()] = $id;

        return ['results' => [$data], 'updated' => (bool)$update];
    }

    /**
     * @param string $id
     * @return array
     * @throws NoIndexesException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function delete(string $id): array
    {
        $delete = $this->connection->delete($this->table->getName(), [$this->getPrimaryKeyName() => $id]);
        $data = [$this->getPrimaryKeyName() => $id];

        return ['results' => [$data], 'deleted' => (bool)$delete];
    }

    /**
     * Sets the default or named table.
     *
     * @param string|null $tableName
     * @throws NoTablesException
     */
    public function setTable($tableName = null): void
    {
        $schemaManager = $this->connection->getSchemaManager();
        $tableNames = $schemaManager->listTableNames();

        if (!$tableNames) {
            throw new NoTablesException('Database has no tables.');
        }

        if ($tableName !== null && in_array($tableName, $tableNames)) {
            $this->table = $schemaManager->listTableDetails($tableName);
        } else {
            $this->table = $schemaManager->listTableDetails($tableNames[0]);
        }
    }

    /**
     * Gets the table columns.
     *
     * @return Column[]
     */
    public function getColumns(): array
    {
        return $this->table->getColumns();
    }

    /**
     * Gets the name of the table's primary key (index).
     *
     * @return string
     * @throws NoIndexesException
     */
    public function getPrimaryKeyName(): string
    {
        $indexes = $this->table->getIndexes();

        if (!$indexes) {
            throw new NoIndexesException('Table "'.$this->table->getName().'" has no indexes.');
        }

        return key($indexes);
    }
}