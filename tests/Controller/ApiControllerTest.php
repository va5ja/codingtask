<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ApiControllerTest
 * @package App\Tests\Controller
 */
class ApiControllerTest extends WebTestCase
{
    /** @var Client */
    protected $client;

    public function setUp()
    {
        parent::setUp();

        $this->client = static::createClient();
    }

    /**
     * Test for missing security token
     */
    public function testMissingSecurityToken()
    {
        $this->client->request(
            'POST',
            '/api/v1/dataset',
            [],
            [],
            [],
            '{}'
        );

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode(), 'Expected response code 400.');
        $this->assertContains('Security token is required.', $this->client->getResponse()->getContent());
    }

    /**
     * Test for invalid security token
     */
    public function testInvalidSecurityToken()
    {
        $this->client->request(
            'POST',
            '/api/v1/dataset',
            [],
            [],
            ['HTTP_X_SECURITY_TOKEN' => '_err'],
            '{}'
        );

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode(), 'Expected response code 400.');
        $this->assertContains('Invalid security token.', $this->client->getResponse()->getContent());
    }

    /**
     * Test an invalid POST request
     */
    public function testInvalidCreateAction()
    {
        $this->client->request(
            'POST',
            '/api/v1/dataset',
            [],
            [],
            [],
            '{"test": "hello"}'
        );

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode(), 'Expected response code 400.');
    }

    /**
     * Test a valid POST request
     */
    public function testValidCreateAction()
    {
        $this->client->request(
            'POST',
            '/api/v1/dataset',
            [],
            [],
            ['HTTP_X_SECURITY_TOKEN' => $this->client->getContainer()->getParameter('api_security_token')],
            '{"title": "Title", "description": "Desc", "short_description": "Short desc", "category": "", "price": "43.6", "image_link": "http://dummyimage.com/162x214.jpg/cc0000/ffffff", "deeplink": "http://thetimes.co.uk"}'
        );

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode(), 'Expected response code 201.');
        $this->assertTrue($this->client->getResponse()->headers->has('Location'));
    }

    /**
     * Test a valid GET request
     */
    public function testValidReadAction()
    {
        $this->client->request(
            'GET',
            '/api/v1/dataset',
            [],
            [],
            ['HTTP_X_SECURITY_TOKEN' => $this->client->getContainer()->getParameter('api_security_token')]
        );

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), 'Expected response code 200.');
    }

    /**
     * Test 'Not Found' PUT action
     */
    public function testNoFoundUpdateAction()
    {
        $this->client->request(
            'PUT',
            '/api/v1/dataset/0',
            [],
            [],
            ['HTTP_X_SECURITY_TOKEN' => $this->client->getContainer()->getParameter('api_security_token')],
            '{"title": "Title", "description": "Desc", "short_description": "Short desc", "category": "", "price": "43.6", "image_link": "http://dummyimage.com/162x214.jpg/cc0000/ffffff", "deeplink": "http://thetimes.co.uk"}'
        );

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode(), 'Expected response code 404.');
    }

    /**
     * Test invalid PUT action
     */
    public function testInvalidUpdateAction()
    {
        $this->client->request(
            'PUT',
            '/api/v1/dataset/3',
            [],
            [],
            ['HTTP_X_SECURITY_TOKEN' => $this->client->getContainer()->getParameter('api_security_token')],
            '{"description": "Desc", "short_description": "Short desc", "category": "", "price": "43.6", "image_link": "http://dummyimage.com/162x214.jpg/cc0000/ffffff", "deeplink": "http://thetimes.co.uk"}'
        );

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode(), 'Expected response code 400.');
    }

    /**
     * Test valid PUT action
     */
    public function testValidUpdateAction()
    {
        $this->client->request(
            'PUT',
            '/api/v1/dataset/3',
            [],
            [],
            ['HTTP_X_SECURITY_TOKEN' => $this->client->getContainer()->getParameter('api_security_token')],
            '{"title": "Title", "description": "Desc", "short_description": "Short desc", "category": "", "price": "43.6", "image_link": "http://dummyimage.com/162x214.jpg/cc0000/ffffff", "deeplink": "http://thetimes.co.uk"}'
        );

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), 'Expected response code 200.');
    }

    /**
     * Test 'Not Found' DELETE action
     */
    public function testNoFoundDeleteAction()
    {
        $this->client->request(
            'DELETE',
            '/api/v1/dataset/0',
            [],
            [],
            ['HTTP_X_SECURITY_TOKEN' => $this->client->getContainer()->getParameter('api_security_token')]
        );

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode(), 'Expected response code 404.');
    }

    /**
     * Test valid DELETE action
     */
    public function testValidDeleteAction()
    {
        $this->client->request(
            'POST',
            '/api/v1/dataset',
            [],
            [],
            ['HTTP_X_SECURITY_TOKEN' => $this->client->getContainer()->getParameter('api_security_token')],
            '{"title": "Title", "description": "Desc", "short_description": "Short desc", "category": "", "price": "43.6", "image_link": "http://dummyimage.com/162x214.jpg/cc0000/ffffff", "deeplink": "http://thetimes.co.uk"}'
        );

        $responseContent = json_decode($this->client->getResponse()->getContent(), true);

        $this->client->request(
            'DELETE',
            '/api/v1/dataset/'.$responseContent['results'][0]['id'],
            [],
            [],
            ['HTTP_X_SECURITY_TOKEN' => $this->client->getContainer()->getParameter('api_security_token')]
        );

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), 'Expected response code 200.');
    }

    /**
     * Test OPTIONS action
     */
    public function testOptionsAction()
    {
        $this->client->request(
            'OPTIONS',
            '/api/v1/dataset'
        );

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), 'Expected response code 200.');
        $this->assertTrue($this->client->getResponse()->headers->has('Allow'));
        $this->assertTrue($this->client->getResponse()->headers->has('Access-Control-Allow-Origin'));
    }
}