<?php

namespace App\Tests\Model;

use App\Model\Dataset;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Types\Type;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

/**
 * Class DatasetTest
 * @package App\Tests\Model
 */
class DatasetTest extends TestCase
{
    /**
     * Test Request validation
     * @throws \Doctrine\DBAL\DBALException
     */
    public function testValidateRequest()
    {
        $dataset = new Dataset();
        $dataset->property1 = 'data1';
        $dataset->property2 = 'data2';
        $dataset->property3 = 'data3';

        $columns = [
            'id' => new Column('property1', Type::getType(Type::TEXT)),
            'property1' => new Column('property1', Type::getType(Type::TEXT)),
            'property2' => new Column('property2', Type::getType(Type::INTEGER)),
        ];
        $dataset->setWhitelistColumns($columns, 'id');

        $errors = $dataset->validateRequest(true);

        $this->assertCount(1, $errors, 'There should be 1 validation error.');
        $this->assertContains('property3', $errors[0], 'Error message should contain "property3".');
    }

    /**
     * Test columns metadata validation
     */
    public function testValidateColumns()
    {
        $validator = Validation::createValidatorBuilder()->addMethodMapping('loadValidatorMetadata')->getValidator();
        $dataset = new Dataset();
        $dataset->property1 = 'data1';
        $dataset->property2 = 'data2';
        $dataset->property3 = 'data3';

        $columns = [
            'id' => new Column('property1', Type::getType(Type::TEXT)),
            'property1' => new Column('property1', Type::getType(Type::TEXT)),
            'property2' => new Column('property2', Type::getType(Type::INTEGER)),
        ];
        $dataset->setWhitelistColumns($columns, 'id');

        $errors = $validator->validate($dataset);

        $this->assertCount(1, $errors, 'There should be 1 validation error.');
        $this->assertContains('property2', $errors[0]->getMessage(), 'Error message should contain "property2".');
    }
}